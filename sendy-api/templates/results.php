<!DOCTYPE html>

<html>
<head>
    <style>
        .token_result {
            background-color: aqua;
            margin: 5px 0;
        }
    </style>
</head>

<body>
    <h1>Results</h1>
    <div>
        <strong>VerifyDomainIdentity</strong>:&nbsp;<span class="token_result"><?=$verificationToken?></span>
    </div>

    <hr>

    <div>
        <strong>VerifyDomainDkim</strong>:
        <ul>
            <?php foreach ($dkimTokens as $token):?>
                <li><span class="token_result"><?=$token?></span></li>
            <?php endforeach; ?>
        </ul>
    </div>

    <br><br>
    <hr>
    <div>
        <a href="index.php">New Request</a>
    </div>
</body>
</html>