<?php
require 'libs/AWS/aws-autoloader.php';

$credentials = require('../creds.php');

$s3 = new Aws\Ses\SesClient([
    'version' => 'latest',
    'region'  => 'us-east-1',
    'credentials' => $credentials,
]);

function getProcessedTemplate($templateName, $vars=[])
{
    extract($vars);

    ob_start();

    require($templateName);

    $output = ob_get_contents();

    ob_end_clean();

    return $output;
}