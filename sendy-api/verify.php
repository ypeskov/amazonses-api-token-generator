<?php

require_once 'common.php';

if (!isset($_GET['domain'])) {
    header('Location: index.php');
    die();
}

$domain = $_GET['domain'];

try {
    $result = $s3->verifyDomainIdentity([
        'Domain' => $domain
    ]);

    $verificationToken = $result->toArray()['VerificationToken'];
} catch (Exception $e) {
    $verificationToken = "Error during verification";
}

$fileLines = [];
$fileLines[] = "Domain verification record set:";
$fileLines[] = 'Record name,Record type,Record Value';
$fileLines[] = '_amazonses.'.$domain.','.'TXT'.','.$verificationToken;
$fileLines[] = '';
$fileLines[] = '';

try {
    $result = $s3->verifyDomainDkim([
        'Domain' => $domain
    ]);

    $dkimTokens = $result->toArray()['DkimTokens'];
} catch (Exception $e) {
    $dkimTokens[] = 'Error during verification';
}

$fileLines[] = 'DKIM record set:';
$fileLines[] = 'Record name,Record type,Record Value';
foreach ($dkimTokens as $dkimToken) {
    $fileLines[] = $dkimToken.'._domainkey.'.$domain.',CNAME,'.$dkimToken.'.dkim.amazonses.com';
}

header('Content-Disposition: attachment; filename="tokens.csv"');



foreach ($fileLines as $line) {
    echo $line."\n";
}

//echo getProcessedTemplate('templates/results.php', [
//    'VerificationToken' => $VerificationToken,
//    'dkimTokens' => $dkimTokens,
//]);